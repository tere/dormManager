let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let fs = require('fs');
let cors = require('cors');


require("./server/utils/mongoConfig")();

//init mongo models;
let models = './server/models';
fs.readdir(models, (err, files) => {
	files.forEach(file => {
		require(models + "/" + file);
	})
})



let app = express();


// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors('*'));


require('./server/api/main')(app);
require('./server/api/seeds');

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	res.status(404).json({
		code: 404,
		message: 'Not found'
	})
});




// // error handler
// app.use(function (err, req, res, next) {
// 	// set locals, only providing error in development
// 	res.locals.message = err.message;
// 	res.locals.error = req.app.get('env') === 'development' ? err : {};
// 	console.log(err);
// 	// render the error page
// 	res.status(err.status || 500);
// 	res.render('error');
// });

module.exports = app;
