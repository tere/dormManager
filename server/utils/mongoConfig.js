/*eslint-env node, express*/
'use strict';

const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
const mongoAppDbName = "dormManager";
const mongo = {
	creds: 'andreiterecoasa:Tere-1993',
	hosts: [
		's129780.mlab.com:29780/proiect',
	]
}

//andreiterecoasa - Tere-1993
// let mongoURI = `mongodb://${mongo.creds}@${mongo.hosts[0]}/${mongoAppDbName}?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin`;
let mongoURI = `mongodb://cosminivan:proiect@ds129780.mlab.com:29780/proiect`;


module.exports = () => {
	console.log(`Attempting to connect to mongo instance`)
	const connectionPromise = mongoose.connect(mongoURI);

	connectionPromise.then((db) => {
		console.log(`Succesfully connected to mongo instance`);
	}).catch((err) => {
		console.error(`Could not connect to mongo ${JSON.stringify(err, null, 4)}`);
		mongoose.disconnect();
	});
};