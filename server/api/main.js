"use strict";
var express = require('express');
var router = express.Router();

module.exports = (app) => {
	require("./user/routes")(app, router);
	require("./fine/routes")(app, router);
	require("./bill/routes")(app, router);
	require("./payment/routes")(app, router);
	app.use("/api", router);
}
