let Users = require('./users');
let doResponse = require('../../utils/common').doResponse;

module.exports = (app, router) => {
	router.get('/user/all/:floor?', (req, res) => {
		Users.getAll(req.params.floor).then(users => {
			return res.send(doResponse(users));
		}).catch(reason => {
			return res.send(doResponse(reason.code, reason.message));
		});
	});
	router.post('/user/edit', (req, res) => {
		Users.editUser(req.body.user).then(savedUser => {
			return res.send(doResponse(savedUser));
		}).catch(reason => {
			return res.send(doResponse(reason.code, reason.message));
		});
	});
	router.post('/user/create', (req, res) => {
		Users.save(req.body.user).then(savedUser => {
			return res.send(doResponse(savedUser));
		}).catch(reason => {
			return res.send(doResponse(reason.code, reason.message));
		});
	});
	router.delete('/user/:email', (req, res) => {
		Users.deleteUser(req.params.email).then(() => {
			return res.send(doResponse(null));
		}).catch(reason => {
			return res.send(doResponse(reason.code, reason.message));
		});
	});
	router.post('/user/import', (req, res) => {
		Users.importBulk(req.body.users).then(savedUsers => {
			return res.send(doResponse(savedUsers));
		}).catch(reason => {
			return res.send(doResponse(reason.code, reason.message));
		});
	});
	router.post('/user/register', (req, res) => {
		Users.register(req.body.user).then(registeredUser => {
			return res.send(doResponse(registeredUser));
		}).catch(reason => {
			return res.send(doResponse(reason.code, reason.message));
		});
	});
	router.post('/user/login', (req, res) => {
		Users.login(req.body.userLogin).then(loginDetails => {
			return res.send(doResponse(loginDetails));
		}).catch(reason => {
			return res.send(doResponse(reason.code, reason.message));
		});
	});
	router.post(`/user/token`, (req, res) => {
		Users.getResetPasswordToken(req.body.changePasswordRequest).then(email => {
			return res.send(doResponse(email));
		}).catch(reason => {
			return res.send(doResponse(reason.code, reason.message));
		});
	});
	router.post(`/user/password`, (req, res) => {
		Users.changePassword(req.body.changePasswordRequest).then(user => {
			return res.send(doResponse(email));
		}).catch(reason => {
			return res.send(doResponse(reason.code, reason.message));
		});
	})
}