const UserModel = require('../../models/user');
const md5 = require('md5');

UserModel.findOne({ email: 'admin@lse.org.ro' }).exec().then(user => {
	const userSeed = Date.now();
	if (!user) {
		UserModel.create({
			name: 'Administrator',
			type: {
				floor: 0,
				role: 'admin'
			},
			email: 'admin@lse.org.ro',
			phone: '-',
			roome: '004',
			seed: userSeed,
			password: md5(`${userSeed}test`),
			valid: true
		})
	} else {
		UserModel.findOneAndUpdate(
			{ email: 'admin@lse.org.ro' },
			{
				$set: {
					seed: userSeed,
					password: md5(`${userSeed}test`),
				}
			},
			{ new: true }
		).exec().then(savedUser => {
			console.log(`Admin user set correctly`);
		})
	}
})