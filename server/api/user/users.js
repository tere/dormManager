const md5 = require('md5');
const UserModel = require('../../models/user');
const TokenModel = require('../../models/token');
const jwt = require('jsonwebtoken');
const passphrase = 'duamnafirea';
const nodemailer = require('nodemailer');
const mailer = nodemailer.createTransport({
	host: 'mail.lse.org.ro',
	port: 26,
	secure: false, // upgrade later with STARTTLS
	auth: {
		user: 'andrei.terecoasa@lse.org.ro',
		pass: 'Tere-1993'
	},
	tls: {
		// do not fail on invalid certs
		rejectUnauthorized: false
	}
});



const User = {
	getAll: (floor) => {
		return new Promise((resolve, reject) => {
			let conditions = {};
			if (floor) {
				conditions = {
					type: {
						floor: floor
					}
				}
			}
			UserModel.find(conditions).exec().then(users => {
				return resolve(users);
			}).catch(reason => {
				return reject({
					code: 501,
					message: "Can't list"
				});
			});
		});
	},
	save: (user) => {
		return new Promise((resolve, reject) => {
			if (!user) {
				return reject({
					code: 500,
					message: 'No user object'
				});
			}
			UserModel.create(user).then(newUser => {
				return resolve(newUser);
			}).catch(reason => {
				return reject({
					code: 501,
					message: 'Could not create user'
				});
			});
		});
	},
	importBulk: (users) => {
		return new Promise((resolve, reject) => {
			UserModel.insertMany(users, (err, newUsers) => {
				return resolve(newUsers);
			}).catch(reason => {
				return reject({
					code: 500,
					message: 'Could not create users'
				});
			});
		});
	},
	editUser: (user) => {
		return new Promise((resolve, reject) => {
			UserModel.findOneAndUpdate({
				email: user.email
			}, user, { new: true }, (err, savedUser) => {
				if (err) {
					return reject({
						code: 500,
						message: 'Could not save user'
					});
				}
				return resolve(savedUser);
			});
		});
	},
	deleteUser: (userEmail) => {
		return new Promise((resolve, reject) => {
			UserModel.findOneAndRemove({ email: userEmail }).exec().then(() => {
				console.log()
				return resolve(null);
			}).catch(reason => {
				return reject({
					code: 500,
					message: 'Could not delete user'
				});
			});
		});
	},
	register: (user) => {
		return new Promise((resolve, reject) => {
			user.seed = Date.now();
			user.password = md5(user.seed + user.password);
			UserModel.create(user).then(newUser => {
				return resolve(newUser);
			}).catch(reason => {
				console.log(`Error: `, reason);
				return reject({
					code: 501,
					message: 'Could not register user'
				});
			});
		});
	},
	login: (userLogin) => {
		return new Promise((resolve, reject) => {
			UserModel.findOne({ email: userLogin.email }).exec().then(user => {
				if (!user) {
					return reject({
						code: 404,
						message: 'User not found'
					});
				}
				if (md5(user.seed + userLogin.password) !== user.password) {
					return reject({
						code: 401,
						message: 'Incorrect credentials'
					});
				}
				delete user.seed;
				delete user.password;
				return resolve({
					token: jwt.sign({
						user
					}, passphrase, { expiresIn: '1h' }),
					user
				});
			}).catch(reason => {
				return reject({
					code: 500,
					message: 'Server side error'
				});
			});
		});
	},
	authorize: (roles) => {
		return (req, res, next) => {
			const token = req.body.token || req.query.token || req.headers['authorization'] || req.headers['x-access-token'];
			if (token) {
				console.log("haz token");
				// verifies secret and checks exp
				jwt.verify(token, passphrase, function (err, decoded) {
					if (err) {
						console.log(`Token error: ${err}`);
						return res.send({ code: 403, message: 'Invalid login.' })
					} else {
						// if everything is good, save to request for use in other routes
						UserModel.findOne({
							email: decoded.user.email
						}).exec().then(user => {
							if (user.type.role !== decoded.user.type.role) {
								return res.send({ code: 403, message: 'Invalid login.' });
							}
							if (roles.indexOf(user.type.role) == -1) {
								return res.send({ code: 403, message: `Unauthorized.` });
							}
							req.decoded = decoded;
							return next();
						});
					}
				});
			} else {
				// if there is no token
				// return an error
				return res.send({ code: 403, message: "Unauthorized" })
			}
		}
	},
	getResetPasswordToken: (changePasswordRequest) => {
		console.log(changePasswordRequest);
		return new Promise((resolve, reject) => {
			UserModel.findOne({ email: changePasswordRequest.email }).exec().then(user => {
				if (!user) {
					return reject({
						code: 404,
						message: `No such user`
					});
				}
				const tokenValue = User.getToken();
				const token = {
					token: tokenValue,
					userEmail: changePasswordRequest.email,
					type: 'password_change'
				}
				console.log(user);
				TokenModel.create(token).then((newToken) => {

					const message = {
						to: `${user.name} <${user.email}`,

						// Subject of the message
						subject: 'Token Resetare Parola ✔',

						// from
						from: `Administrare LEU A <leua@lse.org.ro>`,

						// plaintext body
						text: `Token resetare parola Administrare Leu A: ${tokenValue}`,

						// HTML body
						html: `<p>Token resetare parola Administrare Leu A: <b>${tokenValue}</b></p>`

					}

					mailer.sendMail(message, (err, info) => {
						if (err) {
							return reject({
								code: 501,
								message: `Mailul nu a putut fi trimis`
							});
						}
						return resolve(user.email);
					});
				});
			});
		});
	},
	changePassword: (changePasswordRequest) => {
		return new Promise((resolve, reject) => {
			console.log(changePasswordRequest);
			UserModel.findOne({ email: changePasswordRequest.email }).exec().then(user => {
				if (!user) {
					return reject({
						code: 404,
						message: 'No such user'
					});
				}
				TokenModel.findOne({
					token: changePasswordRequest.token,
					type: 'password_change',
					userEmail: changePasswordRequest.email
				}).exec().then(token => {
					if (!token) {
						return reject({
							code: 501,
							message: 'Invalid token'
						});
					}
					TokenModel.findOneAndRemove({
						token: changePasswordRequest.token,
						type: 'password_change',
						userEmail: changePasswordRequest.email
					}).exec();
					const np = md5(`${user.seed}${changePasswordRequest.password}`);
					UserModel.findOneAndUpdate({ email: changePasswordRequest.email }, { $set: { password: np } }, { new: true }, (err, savedUser) => {
						if (err) {
							return reject({
								code: 500,
								message: 'Could not save new password'
							});
						}
						return resolve(savedUser);
					});
				}).catch(reason => {
					return reject({
						code: 500,
						message: 'Could not save token'
					});
				})
			}).catch(reason => {
				return reject({
					code: 500,
					message: 'Error retrieving data for user'
				});
			});
		});
	},
	getToken: () => {
		return Math.random().toString(36).substring(2, 6) + Math.random().toString(36).substring(2, 6);
	}
}


module.exports = User;