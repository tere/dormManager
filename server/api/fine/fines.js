const FineModel = require('../../models/fine');

const Fine = {
	saveFine: (fine) => {
		return new Promise((resolve, reject) => {
			if (fine.hasOwnProperty("_id")) {
				console.log(fine);
				console.log(fine.paid, typeof fine.paid);
				
				FineModel.findOneAndUpdate(
					{ _id: fine._id },
					fine,
					{ new: true }
				).exec().then(updatedFine => {
					console.log(updatedFine);
					return resolve(updatedFine);
				}).catch(reason => {
					return reject({
						code: 500,
						message: `Could not update fine`
					});
				});
			} else {
				FineModel.create(fine).then(newFine => {
					return resolve(newFine);
				}).catch(reason => {
					return reject({
						code: 500,
						message: `Could not add fine`
					})
				})
			}
		});
	},
	getAll: () => {
		return new Promise((resolve, reject) => {
			FineModel.find().populate('_user').exec().then(fines => {
				return resolve(fines);
			}).catch(reason => {
				return reject({
					code: 500,
					message: `Could not get fines`
				});
			});
		});
	}
}


module.exports = Fine;