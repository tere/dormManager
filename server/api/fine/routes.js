let Fines = require('./fines');
let doResponse = require('../../utils/common').doResponse;

module.exports = (app, router) => {
	router.get('/fine/all', (req, res) => {
		Fines.getAll().then(fines => {
			return res.send(doResponse(fines));
		}).catch(reason => {
			return res.send(doResponse(reason.code, reason.message));
		});
	});
	router.post('/fine', (req, res) => {
		Fines.saveFine(req.body.fine).then(newFine => {
			return res.send(doResponse(newFine));
		}).catch(reason => {
			return res.send(doResponse(reason.code, reason.message));
		});
	});
}