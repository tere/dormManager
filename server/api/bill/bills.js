const BillModel = require('../../models/bill');
const PaymentModel = require('../../models/payment');

const Bills = {
    getAll: () => {
        return new Promise((resolve, reject) => {
            BillModel.find().exec().then(bills => {
                return resolve(bills);
            }).catch(reason => {
                return reject({
                    code: 500,
                    message: `Could not get bills`
                });
            });
        })
    },
    saveBill: (bill) => {
        return new Promise((resolve, reject) => {
            BillModel.create(bill).then(createdBill => {
                return resolve(createdBill);
            }).catch(reason => {
                return reject({
                    code: 500,
                    message: `Could not save bill`
                });
            });
        });
	},
	deleteBill: (billId) => {
		return new Promise((resolve, reject) => {
			BillModel.find({ _id: billId }).remove().exec().then(() => {
				return resolve();
			}).catch(reason => {
				return reject({
                    code: 500,
                    message: `Could not delete bill`
                });
			});
		});
	},
	getUserUnpaidBills: (userId) => {
		return new Promise((resolve, reject) => {
			PaymentModel.find({
				user: userId
			}).exec().then(bills => {
				if(!bills) {
					return resolve([]);
				}
				bills = bills.map(b => b._id);
				BillModel.find({
					_id: { $nin: bills }
				}).exec().then(unapdiBills => {
					return resolve(unapdiBills);
				}).catch(reason => {
					return reject({
						code: 500,
						message: `Could not get unpaid bills`
					});
				});
			}).catch(reason => {
				return reject({
                    code: 500,
                    message: `Could not get paid bills`
                });
			});
		});
	}
}

module.exports = Bills;