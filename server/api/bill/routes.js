let Bills = require('./bills');
let Users = require('../user/users');
let doResponse = require('../../utils/common').doResponse;

module.exports = (app, router) => {
	router.get('/bill/all', Users.authorize(['admin']), (req, res) => {
        Bills.getAll().then(bills => {
            return res.send(doResponse(bills));
        }).catch(reason => {
            return res.send(doResponse(reason.code, reason.message));
        });
	});
	router.post('/bill', Users.authorize(['admin']), (req, res) => {
		Bills.saveBill(req.body.bill).then(newBill => {
            return res.send(doResponse(newBill));
        }).catch(reason => {
            return res.send(doResponse(reason.code, reason.message));
        });
	});
	router.delete(`/bill/:billId`, Users.authorize(['admin']), (req, res) => {
		Bills.deleteBill(req.params.billId).then(() => {
			return res.send(doResponse(null));
        }).catch(reason => {
            return res.send(doResponse(reason.code, reason.message));
        });
	});
	router.get(`/bill/unpaid`, Users.authorize(['admin', 'floor_admin', 'tenant']), (req, res) => {
		Bills.getUserUnpaidBills(req.decoded.user._id).then(bills => {
			return res.send(doResponse(bills));
		}).catch(reason => {
            return res.send(doResponse(reason.code, reason.message));
        });
	})
}