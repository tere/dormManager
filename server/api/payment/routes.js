let Payments = require('./payments');
let doResponse = require('../../utils/common').doResponse;

module.exports = (app, router) => {
	router.get('/payment/all', (req, res) => {
        Payments.getAll().then(payments => {
            return res.send(doResponse(payments));
        }).catch(reason => {
            return res.send(doResponse(reason.code, reason.message));
        });
	});
	router.post('/payment', (req, res) => {
		Payments.savePayment(req.body.payment).then(newPayment => {
            return res.send(doResponse(newPayment));
        }).catch(reason => {
            return res.send(doResponse(reason.code, reason.message));
        });
	});
}