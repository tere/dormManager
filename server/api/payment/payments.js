const PaymentModel = require('../../models/payment');

const Payments = {
    getAll: () => {
        return new Promise((resolve, reject) => {
            PaymentModel.find().populate("_user _bill").exec().then(payments => {
                return resolve(payments);
            }).catch(reason => {
                return reject({
                    code: 500,
                    message: `Could not get payments`
                });
            });
        })
    },
    savePayment: (payment) => {
        return new Promise((resolve, reject) => {
            PaymentModel.create(payment).then(createdPayment => {
                return resolve(createdPayment);
            }).catch(reason => {
				if(reason.errmsg.indexOf('duplicate key error collection') > 0) {
					return reject({
						code: 501,
						message: `Plata s-a efectuat deja`
					});
				}
                return reject({
                    code: 500,
                    message: `Could not save bill`
                });
            });
        });
	}
}

module.exports = Payments;