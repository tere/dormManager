'use strict';
const mongoose = require('mongoose');

const CommentModel = new mongoose.Schema({
	comment: {
        type: String,
        required: true
	},
	room: {
		type: mongoose.SchemaTypes.ObjectId,
	},
}, {
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true
	}
});


mongoose.Schema.Types.User = CommentModel;
module.exports = mongoose.model('Comment', CommentModel, 'Comment');