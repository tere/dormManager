'use strict';
const mongoose = require('mongoose');

const PaymentModel = new mongoose.Schema({
	bill: {
		type: mongoose.SchemaTypes.ObjectId,
		required: true
	},
	user: {
		type: mongoose.SchemaTypes.ObjectId,
		required: true
	},
}, {
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true
	}
});
PaymentModel.virtual("_user", {
	localField: 'user',
	foreignField: '_id',
	justOne: true,
	ref: 'User'
});
PaymentModel.virtual("_bill", {
	localField: 'bill',
	foreignField: '_id',
	justOne: true,
	ref: 'Bill'
});

PaymentModel.index({
	bill: 1,
	user: 1,
}, { unique: true} );

mongoose.Schema.Types.Payment = PaymentModel;
module.exports = mongoose.model('Payment', PaymentModel, 'Payment');