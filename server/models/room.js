'use strict';
const mongoose = require('mongoose');

const RoomModel = new mongoose.Schema({
	number: {
		type: String,
		required: true
	},
	floor: {
		type: Number,
		required: true
	}
}, {
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true
    }
});



mongoose.Schema.Types.Room = RoomModel;
module.exports = mongoose.model('Room', RoomModel, 'Room');