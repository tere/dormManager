'use strict';
const mongoose = require('mongoose');

const FineModel = new mongoose.Schema({
	value: {
        type: Number,
        required: true
	},
	email: {
		type: String,
	},
	reason: {
		type: String,
		required: true
	},
	paid: {
		type: Boolean,
		required: false
	},
	paidOn: {
		type: Date,
		required: false
	}
}, {
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true
	},
	timestamps: {
        createdAt: 'issuedAt'
    }
});


FineModel.virtual("_user", {
	localField: 'email',
	foreignField: 'email',
	ref: 'User',
	justOne: true
});

mongoose.Schema.Types.Fine = FineModel;
module.exports = mongoose.model('Fine', FineModel, 'Fine');