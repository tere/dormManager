'use strict';
const mongoose = require('mongoose');

const UserTypeSchema = new mongoose.Schema({
	floor: {
		type: Number,
		required: false
	},
	role: {
		type: String,
		required: true,
		enum: ['admin', 'tenant', 'floor_admin'],
		default: 'tenant'
	}
})

const UserModel = new mongoose.Schema({
	name: {
        type: String,
        required: false
    },
	type: {
		type: UserTypeSchema,
		required: true,
		default: {
			floor: 0
		}
	},
    email: {
        type: String,
		required: true,
		unique: true
    },
    phone: {
        type: String,
	},
	room: {
		type: String
	},
	password: {
		type: String,
		required: true
	},
	seed: {
		type: String,
		required: true
	},
	valid: {
		type: Boolean,
		required: true,
		default: false
	},
	checkedIn: {
		type: Boolean,
		required: false,
		default: true
	},
	checkInDate: {
		type: Date,
		required: false,
		default: Date.now
	},
	checkOutDate: {
		type: Date,
		required: false
	},
	exempted: {
		type: Boolean,
		default: false,
		required: false
	}
}, {
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true
    }
});


// UserModel.virtual("_votes", {
// 	ref: 'Vote',
// 	localField: '_id',
// 	foreignField: 'userId',
// 	justOne: false
// })

mongoose.Schema.Types.User = UserModel;
module.exports = mongoose.model('User', UserModel, 'User');