'use strict';
const mongoose = require('mongoose');

const TokenModel = new mongoose.Schema({
	token: {
		type: String,
		required: true
	},
	userEmail: {
		type: String,
		required: true
	},
	type: {
		type: String,
		required: true
	}
}, {
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true
    }
});



mongoose.Schema.Types.Token = TokenModel;
module.exports = mongoose.model('Token', TokenModel, 'Token');