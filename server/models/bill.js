'use strict';
const mongoose = require('mongoose');

const BillModel = new mongoose.Schema({
	name: {
        type: String,
        required: true
    },
	value: {
        type: String,
        required: true
    },
	due: {
		type: Date,
		require: true
	}
}, {
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true
    }
});


// UserModel.virtual("_votes", {
// 	ref: 'Vote',
// 	localField: '_id',
// 	foreignField: 'userId',
// 	justOne: false
// })

mongoose.Schema.Types.User = BillModel;
module.exports = mongoose.model('Bill', BillModel, 'Bill');